package hr.vub;

public class Container<T> {
    private T variable;
    Container(T var){
        this.variable = var;
    }
    T get(){
        return variable;
    }
    void set(T var){
        this.variable = var;

    }
}
