package hr.vub;

public class Main {

    public static void main(String[] args) {

        //String container
        Container<String> string = new Container<String>("String container");
        System.out.println(string.get());
        string.set("Change");
        System.out.println(string.get());

        //Integer container
        Container<Integer> integer = new Container<Integer>(5);
        System.out.println(integer.get());
        integer.set(10);
        System.out.println(integer.get());

        //String container
        Container<Float> floatNum = new Container<Float>(3.14f);
        System.out.println(floatNum.get());
        floatNum.set(5.23f);
        System.out.println(floatNum.get());

        /*
        Calculator<Integer> calc = new Calculator<>();
        System.out.println(calc.Add(2, 5));
        System.out.println(calc.Substract(2, 5));
        System.out.println(calc.Multiply(2, 5));
        System.out.println(calc.Divide(2, 5));

        Calculator<Double> calcD = new Calculator<>();
        System.out.println(calcD.Add(2.0, 5.0));
        System.out.println(calcD.Substract(2.4, 5.1));
        System.out.println(calcD.Multiply(2.6, 5.7));
        System.out.println(calcD.Divide(2.3, 5.8));

        */
    }
}
