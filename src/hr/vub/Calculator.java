package hr.vub;

import org.jetbrains.annotations.NotNull;

public class Calculator<T extends Number> {

    public int Add(@NotNull T o1, @NotNull T o2){
        return o1.intValue() + o2.intValue();
    }

    public int Substract(T o1, T o2) {
        return o1.intValue() - o2.intValue();
    }

    public int Divide(T o1, T o2){
        if(o2.intValue() == 0){
            return  0;
        } else {
            return o1.intValue()/o2.intValue();
        }
    }

    public int Multiply(T o1, T o2) {
        return o1.intValue() * o2.intValue();
    }

}
